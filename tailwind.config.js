/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
  ],
  theme: {
    extend: {
      backgroundColor: {
        'partoo-blue': '#100c8c',
        'partoo-light-blue': '#0c42cc',
      },
      colors: {
        'partoo-blue': '#100c8c',
        'partoo-light-blue': '#0c42cc',
      }
    },
  },
  plugins: [],
}