<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Storeloc Technical Assessment</title>

        @vite('resources/sass/app.scss')
        @yield('styles')
    </head>
    <body role="main">
        <div class="p-8 mb-8 text-center bg-partoo-blue">
            <h1 class="text-white font-semibold text-2xl lg:text-4xl">
                <a href="{{ route('index') }}">{{ config('app.name') }}</a>
            </h1>
            <h2 class="text-white underline text-lg lg:text-xl">Technical Assessment</h2>
        </div>

        <div class="p-4 max-w-7xl mx-auto">
            @yield('content')
        </div>

        @viteReactRefresh
        @vite('resources/js/app.js')
        @yield('scripts')
    </body>
</html>