@extends('layout')

@section('content')
    <h3 class="text-center font-semibold text-partoo-blue underline text-lg lg:text-2xl">{{ $store->name }}</h3>
    <h4 class="text-center font-semibold text-partoo-blue mb-4 text-md lg:text-xl">Everyting you need to know about this store is here</h4>

    <a href="{{ route('index') }}" class="button"><- Back to list</a>

    <div id="store-container" class="mt-8 lg:mt-16">
        <div class="mb-4">
            <p class="font-bold text-partoo-blue">Address: </p>
            <p class="w-3/5 text-sm leading-none">{{ $store->address }}, {{ $store->zipcode }} {{ $store->city }}, {{ $store->country_code }}</p>
        </div>

        <div class="mb-4">
            <p class="font-bold text-partoo-blue">Opening hours: </p>
            <ul class="grid gap-2 grid-cols-1 list-disc pl-6 lg:grid-cols-2">
                @foreach(json_decode($store->hours, true) as $day => $hours)
                    <li>
                        <span class="font-semibold">{{ $day }}:</span>

                        <p class="w-full text-sm leading-none lg:w-3/5">
                                Morning: {{ $hours[0] ?? 'Closed' }}
                        </p>

                        <p class="w-full text-sm leading-none lg:w-3/5">
                                Afternoon: {{ $hours[1] ?? 'Closed' }}
                        </p>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="mb-4">
            <p class="font-bold text-partoo-blue">Is this store open right now ? <span class="{{ $isOpen ? 'text-green-500' : 'text-red-500' }}">{{ $isOpen ? 'Yes' : 'No' }}</span></p>
        </div>

        <div class="mb-4">
            <p class="font-bold text-partoo-blue">Description: </p>
            <p>{{ $store->description }}</p>
        </div>

        <div class="mb-4">
            <p class="font-bold text-partoo-blue">Pictures: </p>

            <div class="grid grid-cols-2 gap-2 place-items-center lg:grid-cols-5">
                @foreach ($store->pictures as $picture)
                    <img src="{{ $picture->picture_path }}" alt="{{ $store->name }} alt" class="object-cover w-full h-full" loading="lazy" />
                @endforeach
            </div>
        </div>

        <div id="services-container">
            <p class="font-bold text-partoo-blue">Services: </p>
            <ul class="grid gap-2 grid-rows-1 list-disc pl-6 lg:grid-rows-3">
                @foreach($store->services as $service)
                    <li>
                        <span class="font-semibold">{{ ucfirst($service->name) }}:</span>
                        <p class="w-full text-sm leading-none lg:w-3/5">{{ $service->description }}</p>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection