@extends('layout')

@section('styles')
    <link
        href="https://cdn.jsdelivr.net/npm/tom-select/dist/css/tom-select.css"
        rel="stylesheet"
    />
@endsection

@section('content')
    <h3 class="text-center font-semibold text-partoo-blue underline text-lg lg:text-2xl">Where is my store ?</h3>
    <h4 class="text-center font-semibold text-partoo-blue mb-4 text-md lg:text-xl">Here, you will find the list of every store within the range you'll specify below</h4>

    <div id="search-container"></div>
@endsection