import { useEffect, useState } from "react";
import { useSRContext } from "../contexts/SearchResultsProvider";

const Results = () => {
    const { criteria, isFormSubmitted, setIsFormSubmitted } = useSRContext();
    const [results, setResults] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [lastPage, setLastPage] = useState(1);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (isFormSubmitted) {
            setLoading(true);
            
            fetchData(currentPage);

            setIsFormSubmitted(false);
        }
    }, [isFormSubmitted, setIsFormSubmitted, currentPage]);

    useEffect(() => {
        if (loading) {
            fetchData(currentPage);
        }
    }, [currentPage]);

    const fetchData = async (page) => {
        try {
            const response = await axios.get(`/results?page=${page}`, {
                params: { criteria }
            });

            setResults(response.data.data);
            setLastPage(response.data.meta.last_page);
        } catch (error) {
            console.error("Error fetching results:", error);
        } finally {
            setLoading(false);
        }
    };

    const handleButtonClick = (direction) => {
        setLoading(true);

        if (direction === "prev" && currentPage > 1) {
            setCurrentPage(currentPage - 1);

            return;
        }

        setCurrentPage(currentPage + 1);
    };

    return (
        <>
            <h4 className="text-center font-semibold text-partoo-blue underline text-lg text-2xl mb-4">Results</h4>
            <div className="pagination mb-8">
                <button type="button" onClick={() => handleButtonClick("prev")}>Previous</button>
                <small>Page {currentPage} of {lastPage}</small>
                <button type="button" onClick={() => handleButtonClick("next")}>Next</button>
            </div>
            {
                loading 
                ?
                    <p className="text-center">Loading...</p>
                : 
                    results.length > 0
                    ?
                        <div id="results-container">
                            {results.map((result) => (
                                <div key={`${result.name}-${result.lat}-${result.lng}`} className="card" onClick={() => window.location.href = `/results/${result.id}/details`}>
                                    <h5 className="text-center mb-2">{result.name}</h5>

                                    <div className="grid place-content-center gap-2 grid-cols-2 mb-4">
                                        <small>Address: {result.address}</small>
                                        <small>City: {result.city}</small>
                                    </div>

                                    <ul className="grid place-content-center gap-2 grid-cols-2">
                                        <p className="font-bold underline">Services:</p>
                                        {
                                            result.services.map((service) => (
                                                <li key={service.id}>{service.name}</li>
                                            ))
                                        }
                                    </ul>
                                </div>
                            ))}
                        </div>
                    :
                    <p className="text-center">No results yet, did you hit the Search button ?</p>
            }
        </>
    );
};

export default Results;