import React from "react";
import ReactDOM from 'react-dom/client';
import Criteria from "./Criteria";
import Results from "./Results";
import { SearchResultsProvider } from "../contexts/SearchResultsProvider";

export const SearchContainer = () => {
    return (
        <>
            <SearchResultsProvider>
                <Criteria />
                <Results />
            </SearchResultsProvider>
        </>
    )
}

const container = document.getElementById('search-container');

if (container) {
    ReactDOM.createRoot(container).render(<SearchContainer />);
}