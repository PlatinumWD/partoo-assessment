import React, { useEffect, useState } from "react";
import { MultiSelect } from "react-multi-select-component";
import axios from "axios";
import { useSRContext } from "../contexts/SearchResultsProvider";

const Criteria = () => {
    const [services, setServices] = useState([]);
    const [selected, setSelected] = useState([]);
    const { setIsFormSubmitted, criteria, setCriteria } = useSRContext();

    useEffect(() => {
        const fetchServices = async () => {
            const response = await axios.get('/services');

            response.data.data.forEach((service) => {
                setServices((services) => [...services, {label: service.name, value: service.name}]);
            })
        }

        fetchServices();
    }, []);

    useEffect(() => setCriteria({...criteria, services: selected}) , [selected.length])

    const handleSubmit = (e) => {
        e.preventDefault();

        setIsFormSubmitted(true);
    }

    return (
        <div className="grid place-content-center mb-4 lg:mb-12">
            <form onSubmit={handleSubmit} method="GET" action="{{ route('results') }}" className="lg:gap-2">
                <div className="bounds">
                    <div><input type="search" name="n" placeholder="North Latitude" onBlur={(e) => setCriteria({...criteria, north: e.target.value})} /></div>
                    <div><input type="search" name="w" placeholder="West Longitude" onBlur={(e) => setCriteria({...criteria, west: e.target.value})} /></div>
                    <div><input type="search" name="s" placeholder="South Latitude" onBlur={(e) => setCriteria({...criteria, south: e.target.value})} /></div>
                    <div><input type="search" name="e" placeholder="East Longitude" onBlur={(e) => setCriteria({...criteria, east: e.target.value})} /></div>
                </div>
                <div className="filters">
                    <div>
                        <MultiSelect
                            options={services}
                            value={selected}
                            onChange={setSelected}
                            labelledBy="Select services"
                            isLoading={services.length === 0}
                        />
                    </div>
                    <div className="mr-4">
                        <select name="operator" onChange={(e) => setCriteria({...criteria, operator: e.target.value})}>
                            <option value="">Select an operator</option>
                            <option value="OR">OR</option>
                            <option value="AND">AND</option>
                        </select>
                    </div>

                    <button type="submit">Search</button>
                </div>
            </form>
        </div>
    )
}

export default Criteria;