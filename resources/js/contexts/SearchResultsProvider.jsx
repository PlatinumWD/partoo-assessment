import React, { createContext, useContext, useState } from 'react';

const SearchResultsContext = createContext();
export const useSRContext = () => useContext(SearchResultsContext);

export const SearchResultsProvider = ({ children }) => {
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [criteria, setCriteria] = useState({
        north: null,
        west: null,
        south: null,
        east: null,
        services: [],
        operator: null,
    });

    return <SearchResultsContext.Provider value={{ criteria, setCriteria, isFormSubmitted, setIsFormSubmitted }}>
        {children}
    </SearchResultsContext.Provider>;
}