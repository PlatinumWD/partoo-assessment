<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'address' => $this->address,
            'city' => $this->city,
            'zipcode' => $this->zipcode,
            'country_code' => $this->country_code,
            'services' => $this->whenLoaded('services'),
            'pictures' => $this->whenLoaded('pictures'),
        ];
    }
}
