<?php

namespace App\Http\Controllers;

use App\Http\Services\StorelocService;
use App\Models\Store;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StorelocController extends Controller
{
    public function __construct(private StorelocService $storelocService)
    {}

    public function index(): View
    {
        return view('index', [
            'services' => $this->storelocService->getServices(),
        ]);
    }

    public function services(): JsonResource
    {
        return $this->storelocService->getServices();
    }

    public function results(Request $request): JsonResource
    {
        // We could create a form request but it's not necessary for this example
        // Because it's a simple validation with small amount of values
        $validated = $request->validate([
            'criteria' => ['required', 'array'],
            'criteria.services' => ['required', 'array', 'min:1'],
            'criteria.services.*.label' => ['required', 'string'],
            'criteria.services.*.value' => ['required', 'string'],
            'criteria.operator' => ['required', 'string', 'in:AND,OR'],
            'criteria.north' => ['required', 'numeric', 'regex:/^\d*(\.\d{1,2})?$/'],
            'criteria.south' => ['required', 'numeric', 'regex:/^\d*(\.\d{1,2})?$/'],
            'criteria.east' => ['required', 'numeric', 'regex:/^\d*(\.\d{1,2})?$/'],
            'criteria.west' => ['required', 'numeric', 'regex:/^\d*(\.\d{1,2})?$/'],
        ]);

        return $this->storelocService->getStores($validated);
    }

    public function details(Store $store): View|NotFoundHttpException
    {
        if (!$store) {
            throw new NotFoundHttpException('We could\'t find that store in our database.');
        }

        return view('details', [
            'store' => $this->storelocService->getStoreDetails($store),
            'isOpen' => $this->storelocService->isOpen($store->hours),
        ]);
    }
}
