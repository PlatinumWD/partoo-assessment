<?php

namespace App\Http\Services;

use App\Http\Resources\ServiceResource;
use App\Http\Resources\StoreResource;
use App\Models\Service;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class StorelocService
{
    public function getServices(): JsonResource
    {
        return ServiceResource::collection(Service::all());
    }

    public function getStores(array $data = []): JsonResource
    {
        if (empty($data)) {
            throw new \Exception('No criteria provided');
        }

        $stores = [];
        $criteria = $data['criteria'];
        $servicesNames = (new Collection($criteria['services']))->pluck('value')->toArray();

        switch ($criteria['operator']) {
            case 'AND':
                $stores = Store::with('services')
                    ->where('lat', '<=', $criteria['north'])
                    ->where('lat', '<=', $criteria['south'])
                    ->where('lng', '<=', $criteria['east'])
                    ->where('lng', '<=', $criteria['west'])
                    ->whereHas('services', function ($query) use ($servicesNames) {
                        $query->whereIn('name', $servicesNames);
                    }, '=', count($servicesNames))
                    ->orderBy('name')
                    ->get();

                    break;

            case 'OR':
                $stores = Store::with('services')
                    ->where('lat', '<=', $criteria['north'])
                    ->where('lat', '<=', $criteria['south'])
                    ->where('lng', '<=', $criteria['east'])
                    ->where('lng', '<=', $criteria['west'])
                    ->whereHas('services', function ($query) use ($servicesNames) {
                        $query->whereIn('name', $servicesNames);
                    })
                    ->orderBy('name')
                    ->get();

                break;

        }

        return StoreResource::collection($stores);
    }

    public function getStoreDetails(Store $store): JsonResource
    {
        if (Cache::has($store->id)) {
            return Cache::get($store->id);
        }

        $storeDetails = new StoreResource($store->with('services')->first());

        Cache::put($store->id, $storeDetails, 60);

        return $storeDetails;
    }
    
    public function isOpen(string $hours): bool
    {
        $isOpen = false;

        foreach (json_decode($hours, true) as $day => $hours) {
            if ($day !== Carbon::now()->format('l')) {
                continue;
            }

            if (isset($hours[0])) {
                $morningStart = Carbon::createFromDate(explode('-', $hours[0])[0])->format('H:i');
                $morningEnd = Carbon::createFromDate(explode('-', $hours[0])[1])->format('H:i');

                $isOpen = Carbon::now('Europe/Paris')->between($morningStart, $morningEnd);
            }

            if (isset($hours[1])) {
                $afternoonStart = Carbon::createFromDate(explode('-', $hours[1])[0])->format('H:i');
                $afternoonEnd = Carbon::createFromDate(explode('-', $hours[1])[1])->format('H:i');

                $isOpen = Carbon::now('Europe/Paris')->between($afternoonStart, $afternoonEnd);
            }

            break;
        }

        return $isOpen;
    }
}