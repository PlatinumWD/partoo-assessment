<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Service extends Model
{
    use HasFactory, HasUuids;

    protected $guarded = [];

    public function stores(): BelongsToMany
    {
        return $this->belongsToMany(Store::class, 'store_service');
    }
}
