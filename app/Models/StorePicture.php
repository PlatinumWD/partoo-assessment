<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class StorePicture extends Model
{
    use HasFactory, HasUuids;

    public const RANDOM_PICTURES = [
        'https://source.unsplash.com/random/900×700/?fruit',
        'https://source.unsplash.com/random/900×700/?restaurant',
        'https://source.unsplash.com/random/900×700/?club',
        'https://source.unsplash.com/random/900×700/?bar',
        'https://source.unsplash.com/random/900×700/?cafe',
        'https://source.unsplash.com/random/900×700/?building',
        'https://source.unsplash.com/random/900×700/?beach',
        'https://source.unsplash.com/random/900×700/?gaming'
    ];

    protected $guarded = [];

    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class);
    }
}
