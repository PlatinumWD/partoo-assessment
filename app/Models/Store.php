<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Service;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Store extends Model
{
    use HasFactory, HasUuids;

    protected $guarded = [];

    public function services(): BelongsToMany
    {
        return $this->belongsToMany(Service::class, 'store_service');
    }

    public function pictures(): HasMany
    {
        return $this->hasMany(StorePicture::class);
    }
}
